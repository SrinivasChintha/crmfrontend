import React, { Component } from "react";
import { connect } from "react-redux";
import { Router, Switch, Route, Link } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import Login from "./components/login.component";
import Register from "./components/register.component";
import Home from "./components/home.component";
import Profile from "./components/profile.component";
import BoardUser from "./components/board-user.component";
import BoardAdmin from "./components/board-admin.component";

import { logout } from "./actions/auth";
import { clearMessage } from "./actions/message";

import { history } from "./helpers/history";

class App extends Component {
	constructor(props) {
		super(props);
		this.logOut = this.logOut.bind(this);

		this.state = {
			showAdminBoard: false,
			currentUser: undefined,
			isAdmin: false,
		};

		history.listen((location) => {
			props.dispatch(clearMessage()); // clear message when changing location
		});
	}

	componentDidMount() {
		const user = this.props.user;
		if (user) {
			this.setState({
				currentUser: user,
				showModeratorBoard: user.roles.includes("ROLE_MODERATOR"),
				showAdminBoard: user.roles.includes("ROLE_ADMIN"),
				isAdmin: user.roles.includes("ROLE_ADMIN") ? true : false,
			});
		}
	}

	logOut() {
		this.props.dispatch(logout());
	}

	render() {
		const { currentUser, showAdminBoard, isAdmin } = this.state;

		return (
			<Router history={history}>
				<div>
					<nav className="navbar navbar-expand navbar-dark bg-primary">
						<Link to={"/"} className="navbar-brand">
							<b>Allstate</b>
						</Link>
						<div className="navbar-nav mr-auto">
							{showAdminBoard && (
								<li className="nav-item">
									<Link to={"/admin"} className="nav-link">
										Admin DashBoard
									</Link>
								</li>
							)}

							{currentUser && !isAdmin && (
								<li className="nav-item">
									<Link to={"/user"} className="nav-link">
										User DashBoard
									</Link>
								</li>
							)}
							{isAdmin && (
								<li className="nav-item">
									<Link to={"/register"} className="nav-link">
										Create User
									</Link>
								</li>
							)}
						</div>

						{currentUser ? (
							<div className="navbar-nav ml-auto">
								<li className="nav-item">
									<Link to={"/profile"} className="nav-link text-capitalize">
										Welcome {currentUser.fullName}
									</Link>
								</li>
								<li className="nav-item">
									<a href="/login" className="nav-link" onClick={this.logOut}>
										LogOut
									</a>
								</li>
							</div>
						) : (
							<div className="navbar-nav ml-auto">
								<li className="nav-item">
									<Link to={"/login"} className="nav-link">
										Login
									</Link>
								</li>
							</div>
						)}
					</nav>

					<div className="container mt-3">
						<Switch>
							<Route exact path={["/", "/home"]} component={Home} />
							<Route exact path="/login" component={Login} />
							<Route exact path="/register" component={Register} />
							<Route exact path="/profile" component={Profile} />
							<Route path="/user" component={BoardUser} />
							<Route path="/admin" component={BoardAdmin} />
						</Switch>
					</div>
				</div>
			</Router>
		);
	}
}

function mapStateToProps(state) {
	const { user } = state.auth;
	return {
		user,
	};
}

export default connect(mapStateToProps)(App);
