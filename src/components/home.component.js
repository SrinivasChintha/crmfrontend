import React, { Component } from "react";

import UserService from "../services/user.service";

export default class Home extends Component {
	constructor(props) {
		super(props);

		this.state = {
			content: "",
		};
	}

	componentDidMount() {
		UserService.getPublicContent().then(
			(response) => {
				this.setState({
					content: response.data,
				});
			},
			(error) => {
				this.setState({
					content:
						(error.response && error.response.data) ||
						error.message ||
						error.toString(),
				});
			}
		);
	}

	render() {
		return (
			<div className="container">
				<header className="jumbotron">
					<a
						href="http://www.salesforce.com/crm/what-is-crm-infographic/"
						target="_blank"
						rel="noreferrer"
					>
						<img
							src="https://www.salesforce.com/content/dam/web/en_us/www/images/hub/what-is-crm-infographic/what-is-crm.jpg"
							alt="What is CRM Software"
						></img>
					</a>
				</header>
			</div>
		);
	}
}
