import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

class Profile extends Component {
	render() {
		const { user: currentUser } = this.props;

		if (!currentUser) {
			return <Redirect to="/login" />;
		} else if (currentUser.roles) {
			if (currentUser.roles.includes("ROLE_ADMIN")) {
				return <Redirect to="/admin" />;
			} else {
				return <Redirect to="/user" />;
			}
		}

		return (
			<div className="container">
				<header className="jumbotron">
					<h3>
						Welcome{" "}
						<strong className="text-capitalize">{currentUser.username}</strong>{" "}
						!!
					</h3>
				</header>
				<p>
					<strong>Token:</strong> {currentUser.accessToken.substring(0, 20)} ...{" "}
					{currentUser.accessToken.substr(currentUser.accessToken.length - 20)}
				</p>
				<p>
					<strong>Id:</strong> {currentUser.id}
				</p>
				<p>
					<strong>Email:</strong> {currentUser.email}
				</p>
				<strong>Authorities:</strong>
				<ul>
					{currentUser.roles &&
						currentUser.roles.map((role, index) => <li key={index}>{role}</li>)}
				</ul>
			</div>
		);
	}
}

function mapStateToProps(state) {
	const { user } = state.auth;
	return {
		user,
	};
}

export default connect(mapStateToProps)(Profile);
